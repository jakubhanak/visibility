<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Visibility;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::listOwnedByUser();
        return view('pages.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Visibility::categories();

        return view('pages.project.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:3|unique:projects',
            'category' => 'required|integer',
        ]);
        $validated['user_id'] = Auth::user()->id;

        $project = Project::create($validated);

        return redirect("/projects/{$project->id}");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $this->authorize('view', $project);

        $posts = collect(Visibility::posts());

        $posts = $posts->filter(function ($post) use ($project) {
            return in_array($project->category, $post['categories']);
        });

        $posts = $posts->map(function ($post) {
            $post['likes'] = Visibility::getPostLikes($post);
            return $post;
        });

        return view('pages.project.show', compact('posts', 'project'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->authorize('delete', $project);

        $project->delete();

        return redirect('/projects');
    }

    /**
     * Get post data from visibility
     *
     * @param Project $project
     * @param int $postId
     * @return void
     */
    public function post(Project $project, $postId)
    {
        $posts = collect(Visibility::posts());

        $post = $posts->filter(function ($post) use ($project) {
            return in_array($project->category, $post['categories']);
        })->filter(function ($post) use ($postId) {
            return $post['id'] == $postId;
        })->first();

        $post['likes'] = Visibility::getPostLikes($post);

        return view('pages.project.post.show', compact('post', 'project'));
    }
}
