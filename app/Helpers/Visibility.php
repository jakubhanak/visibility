<?php
namespace App\Helpers;

class Visibility
{
    /**
     * Get all categories
     *
     * @return void
     */
    public static function categories()
    {
        return json_decode(file_get_contents('https://visibility.sk/blog/wp-json/wp/v2/categories'), true);
    }

    /**
     * Get all posts
     *
     * @return void
     */
    public static function posts()
    {
        return json_decode(file_get_contents('https://visibility.sk/blog/wp-json/wp/v2/posts'), true);
    }

    /**
     * Get likes for post
     *
     * @param array $post
     * @return void
     */
    public static function getPostLikes($post)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sharedcount.com/v1.0/?apikey=e73f11413dc0708fa6af1c6ed56999f325cea5c4&url={$post['link']}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response);
    }
}
