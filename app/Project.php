<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{
    protected $fillable = ['name', 'category', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeListOwnedByUser($query)
    {
        return $query->where('user_id', Auth::user()->id)->get();
    }
}
