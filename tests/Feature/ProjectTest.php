<?php

namespace Tests\Feature;

use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_non_auth_user_may_not_access_any_project_page()
    {
        $this->withExceptionHandling();

        $project = factory(Project::class)->create();

        $urls = collect([
            "/projects",
            "/projects/create",
            "/projects/{$project->id}"
        ]);

        $urls->each(function ($url) {
            $this->get($url)
                ->assertRedirect('/login');
        });

        $this->delete("/projects/{$project->id}")
            ->assertRedirect("/login");

        $project = factory(Project::class)->make();

        $this->post('/projects', $project->toArray())
            ->assertRedirect('/login');
    }

    /** @test */
    public function auth_user_can_create_projects()
    {
        $this->signIn();
        $project = factory(Project::class)->make();

        $response = $this->post('/projects', $project->toArray());
           
        $this->get($response->headers->get('Location'))
            ->assertSee($project->name);
    }

    /** @test */
    public function auth_user_can_open_only_his_projects()
    {
        $this->withExceptionHandling();

        $this->signIn();
        $project = factory(Project::class)->create(['user_id' => Auth::user()->id]);
        $foreignProject = factory(Project::class)->create();

        $this->get("/projects/{$project->id}")
            ->assertSee($project->name);

        $this->get("/projects/{$foreignProject->id}")
            ->assertStatus(403);
    }

    /** @test */
    public function auth_user_can_see_list_of_his_projects()
    {
        $this->signIn();

        $project1 = factory(Project::class)->create(['user_id' => Auth::user()->id]);
        $project2 = factory(Project::class)->create(['user_id' => Auth::user()->id]);
        $foreignProject = factory(Project::class)->create();

        $this->get("/projects")
            ->assertSee($project1->name)
            ->assertSee($project2->name)
            ->assertDontSee($foreignProject->name);
    }

    /** @test */
    public function auth_user_can_delete_his_projects()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $project = factory(Project::class)->create(['user_id' => Auth::user()->id]);
        $foreignProject = factory(Project::class)->create();

        $this->delete("/projects/{$project->id}");
        $this->get("/projects/{$project->id}")
            ->assertStatus(404);

        $this->delete("/projects/{$foreignProject->id}");
        $this->get("/projects/{$foreignProject->id}")
            ->assertStatus(403);
    }
}
