<?php

namespace Tests\Unit;

use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectUnitTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function project_has_unique_name()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $project = factory(Project::class)->make();

        $this->post('/projects', $project->toArray());
        $this->post('/projects', $project->toArray());

        $this->assertCount(1, Project::all());
    }
}
