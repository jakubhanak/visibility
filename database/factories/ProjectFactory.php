<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'category' => $faker->numberBetween(1, 200),
        'user_id' => $user->id ?? factory('App\User')->create()->id
    ];
});
