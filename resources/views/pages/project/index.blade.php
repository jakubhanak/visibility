@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Projekty</div>

                <div class="card-body text-right">
                    <a href="{{ route('projects.create') }}" class="btn btn-primary">@lang('Create')</a>
                    <table class="table text-left">
                        @forelse($projects as $project)
                            <tr>
                                <td>{{$project->name}}</td>
                                <td><a href="{{ route('projects.show', $project) }}">@lang('Open')</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td>@lang('This collection is empty.')</td>
                            </tr>
                        @endforelse
                    </table>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
