@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{ route('projects.index') }}">Projekty</a> / {{ $project->name }}</div>

                <div class="card-body text-right">
                    <a class="btn btn-light" href="{{ route('projects.destroy', $project->id) }}" onclick="event.preventDefault(); var result = confirm('Naozaj chcete projekt vymazať?');
                            if (result) {
                                document.getElementById('destroy').submit();
                            }
                            ">
                        {{ __('Delete') }}
                    </a>

                    <form id="destroy" action="{{ route('projects.destroy', $project->id) }}" method="POST" style="display: none;">
                        @method('delete')
                        @csrf
                    </form>
                    <table class="table text-left">
                        @forelse($posts as $post)
                            <tr>
                            <td><a href="/projects/{{ $project->id }}/posts/{{ $post['id'] }}">{{ $post['link'] }}</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td>@lang('This collection is empty.')</td>
                            </tr>
                        @endforelse 
                    </table>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
