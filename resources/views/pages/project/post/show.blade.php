@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/projects">Projekty</a> / <a href="/projects/{{ $project->id }}">{{ $project->name }}</a> / {{ $post['title']['rendered'] }}</div>

                <div class="card-body">
                    <h2><a href="{{ $post['link'] }}" target="_blank">{{ $post['title']['rendered'] }}</a></h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('Facebook Likes')</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($post['likes']->Facebook as $key => $like)
                            <tr>
                                <td>@lang($key)</td>
                                <td>{{ $like }}</td>
                            </tr>
                        @empty
                            empty
                        @endforelse 
                        </tbody>
                    </table>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
